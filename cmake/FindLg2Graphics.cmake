# - Try to find Langadia++ 2 Graphics
#
#  This defines...
#
#  LG2GRAPHICS_FOUND - system has Langadia++ 2 Graphics
#  LG2GRAPHICS_INCLUDE_DIR - the Langadia++ 2 Graphics include directory
#  LG2GRAPHICS_LIBRARIES - Link these to use Langadia++ 2 Graphics
#  LG2GRAPHICS_DEFINITIONS - Compiler switches required for using Langadia++ 2 Graphics
#


# Are the values already in cache?
if ( LG2GRAPHIS_INCLUDE_DIR AND LG2GRAPHICS_LIBRARIES )
   set(Lg2Graphics_FIND_QUIETLY TRUE)
endif ( LG2GRAPHIS_INCLUDE_DIR AND LG2GRAPHICS_LIBRARIES )

find_path(LG2GRAPHICS_INCLUDE_DIR NAMES lgglwidget.h
  PATHS
  PATH_SUFFIXES lg2/graphics
)

find_library(LG2GRAPHICS_LIBRARIES NAMES lg2graphics
  PATHS
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Lg2Graphics DEFAULT_MSG LG2GRAPHICS_INCLUDE_DIR LG2GRAPHICS_LIBRARIES )

mark_as_advanced(LG2GRAPHICS_INCLUDE_DIR LG2GRAPHICS_LIBRARIES)

