# - Try to find Langadia++ 2 Image
#
#  This defines...
#
#  LG2IMAGE_FOUND - system has Langadia++ 2 Image
#  LG2IMAGE_INCLUDE_DIR - the Langadia++ 2 Image include directory
#  LG2IMAGE_LIBRARIES - Link these to use Langadia++ 2 Image
#  LG2IMAGE_DEFINITIONS - Compiler switches required for using Langadia++ 2 Image
#


# Are the values already in cache?
if ( LG2IMAGE_INCLUDE_DIR AND LG2IMAGE_LIBRARIES )
   set(Lg2Image_FIND_QUIETLY TRUE)
endif ( LG2IMAGE_INCLUDE_DIR AND LG2IMAGE_LIBRARIES )

find_path(LG2IMAGE_INCLUDE_DIR NAMES lgperlinnoise.h
  PATHS
  PATH_SUFFIXES lg2/image
)

find_library(LG2IMAGE_LIBRARIES NAMES lg2image
  PATHS
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Lg2Image DEFAULT_MSG LG2IMAGE_INCLUDE_DIR LG2IMAGE_LIBRARIES )

mark_as_advanced(LG2IMAGE_INCLUDE_DIR LG2IMAGE_LIBRARIES)

