# - Try to find Langadia++ 2
#
#  This defines...
#
#  LG2_FOUND - system has Langadia++ 2
#  LG2_INCLUDE_DIR - the Langadia++ 2 include directory
#  LG2_LIBRARIES - Link these to use Langadia++ 2
#  LG2_DEFINITIONS - Compiler switches required for using Langadia++ 2
#


# Are the values already in cache?
if ( LG2_INCLUDE_DIR AND LG2_LIBRARIES )
   set(Lg2_FIND_QUIETLY TRUE)
endif ( LG2_INCLUDE_DIR AND LG2_LIBRARIES )

find_path(LG2_INCLUDE_DIR NAMES lgmath.h
  PATHS
  PATH_SUFFIXES lg2/core
)

find_library(LG2_LIBRARIES NAMES langadia2
  PATHS
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Lg2 DEFAULT_MSG LG2_INCLUDE_DIR LG2_LIBRARIES )

mark_as_advanced(LG2_INCLUDE_DIR LG2_LIBRARIES)

