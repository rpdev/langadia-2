#include <QApplication>
#include <QObject>
#include <QtOpenGL>
#include <lgglwidget.h>
#include <lgorthoview.h>

#include "MyGLClass.h"

int main(int argc, char** argv)
{
  QApplication app(argc, argv);
  Lg::Gfx::Widget widget;
  Lg::Gfx::OrthoView* view = new Lg::Gfx::OrthoView(&widget);
  MyGLClass myClass;
  
  myClass.connectToWidget(&widget);
  widget.setView(view);
  widget.setAutoRedraw(true);
  widget.show();
  
  return app.exec();
}
