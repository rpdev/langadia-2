#include <QObject>
#include <QPixmap>
#include <QtOpenGL>
#include <lgglwidget.h>
#include <lgtexture.h>
#include <lgtile.h>
#include <lganimation.h>

class MyGLClass : public QObject
{
  Q_OBJECT

  private:
    
    Lg::Gfx::Texture* texture;
    Lg::Gfx::Tile* tile;
    Lg::Gfx::Animation* animation;
    Lg::Gfx::Tile* animTile;
    
    Lg::Gfx::Widget* widget;
    
  public:
    
    MyGLClass(QObject* parent = 0) : QObject(parent) 
    {
      tile = NULL;
    }
    
    void connectToWidget(Lg::Gfx::Widget* widget)
    {
      // Make sure the widget is active, otherwise loading might fail
      widget->makeCurrent();
      
      // Store the widget. We will need it later.
      this->widget = widget;
      
      // Load a simple texture.
      // The first parameter is the parent. We use the MyGLClass here.
      // When the instance of MyGLClass is destroyed, the texture will go, too.
      // Second parameter is the OpenGL texture id. We load the texture itself using 
      // the GL widget.
      // The last parameter tells the texture to delete the texture on exit.
      // As the texture shall share the texture ID with the tile, we select the texture
      // to clean the image on destruction.
      texture = new Lg::Gfx::Texture(this,
		widget->bindTexture(QPixmap("../share/apps/langadia2/examples/example01/tile.png")), 
		true);

      // Load a tile. A tile is - spoken simply - a improved texture
      // ready for 2D drawing.
      // The parameters are similar to the ones of Texture.
      // However, as the OpenGL texture will be deleted by the texture class, 
      // we pass false as the last parameter.
      tile = new Lg::Gfx::Tile(this, texture->texture(), false);
      
      // Create an animation.
      // Animations are sequenced of tiles. By passing true as the last parameter,
      // the animation progressed itself, using an internal timer.
      animation = new Lg::Gfx::Animation(this, true);
      
      // Animations can depend on one or more tiles.
      // We will use just one tile, with multiple frames inside one picture.
      animTile = new Lg::Gfx::Tile(this,
		widget->bindTexture(QPixmap("../share/apps/langadia2/examples/example01/animation.png")));
      for (int i = 0; i < 10; i++) 
      {
	// For each frame, create an AnimationFrame object.
	// Each of these will use the same tile and duration.
	// However, the displayed rectangle changes.
	animation->frames()->append(new Lg::Gfx::AnimationFrame(this, animTile, 100, QRect(65*i, 0, 65, 59)));
      }
      
      // Last but not least:
      // Connect the paint3D signal of the widget with this object's paint3D slot.
      // Equally, connect the paint2D signal to paint2D slot of this object.
      connect(widget, SIGNAL(paint3D(int)), this, SLOT(paint3D(int)));
      connect(widget, SIGNAL(paint2D(QPainter*, int)), this, SLOT(paint2D(QPainter*, int)));
    }
    
  public slots:
    
    void paint3D(int advance)
    {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      
      // old-school painting using our texture class
      // For better visibility, blend with red
      texture->bindTexture();
      glColor3f(1.0, 1.0, 1.0);
      glBegin(GL_QUADS);
      glTexCoord2d(0.0, 0.0);
      glVertex2f(0.0, 0.0);
      glTexCoord2d(1.0, 0.0);
      glVertex2f(texture->width(), 0.0);
      glTexCoord2d(1.0, 1.0);
      glVertex2f(texture->width(), texture->height());
      glTexCoord2d(0.0, 1.0);
      glVertex2f(0.0, texture->height());
      glEnd();
      
      // The same as above, but with Langadia's Tile class
      tile->draw(100, 0);
      
      // Draw an animation. Easy, not?
      animation->draw(250, 25);
    }
    
    void paint2D(QPainter* painter, int advance)
    {
      // Some text to identify the output.
      painter->drawText( 0, widget->height() - 150, 100, 50, Qt::AlignCenter, "Texture");
      painter->drawText(100, widget->height() - 150, 100, 50, Qt::AlignCenter, "Tile");
      painter->drawText(250, widget->height() - 150, 100, 50, Qt::AlignCenter, "Animation");
    }
    
};
 
