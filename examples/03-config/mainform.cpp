/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "mainform.h"


MainForm::MainForm ( QWidget* parent ) : QWidget ( parent )
{
  QVBoxLayout* layout = new QVBoxLayout(this);
  setLayout(layout);

  textEdit = new QTextEdit(this);
  layout->addWidget(textEdit);

  QWidget* buttons = new QWidget(this);
  layout->addWidget(buttons);
  buttons->setLayout(new QHBoxLayout(buttons));

  updateButton = new QPushButton(this);
  updateButton->setText(tr("Update"));
  buttons->layout()->addWidget(updateButton);

  loadButton = new QPushButton(this);
  loadButton->setText(tr("Load"));
  buttons->layout()->addWidget(loadButton);

  connect(updateButton, SIGNAL(clicked(bool)), this, SLOT(updateOutput(bool)));
  connect(loadButton, SIGNAL(clicked(bool)), this, SLOT(setFromText(bool)));

  setFromText(true);
}

void MainForm::updateOutput ( bool )
{
  Lg::Config config;
  config.writeValue("window", "width", width());
  config.writeValue("window", "height", height());
  config.writeValue("window", "caption", windowTitle());
  textEdit->setText(config.toString());
}

void MainForm::setFromText ( bool )
{
  Lg::Config config;
  config.setRawContent(textEdit->toPlainText());
  resize(config.readValue("window", "width", "800").toUInt(),
	 config.readValue("window", "height", "600").toUInt());
  setWindowTitle(config.readValue(
    "window", "caption", "Langadia++ 2 Configuration Example"));
}

