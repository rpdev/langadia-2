#include <QWidget>
#include <QImage>
#include <QGraphicsScene>
#include <lgperlinnoise.h>
#include <lgmath.h>
#include "ui_mainform.h"

class PerlinNoiseForm : public QWidget
{

  Q_OBJECT

  Ui::MainForm ui;

  public:

    PerlinNoiseForm(QWidget* parent = 0);

  public slots:
    void generate();
};