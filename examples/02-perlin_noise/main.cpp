#include <QApplication>
#include <QObject>
#include <QWidget>
#include "pnform.h"

int main(int argc, char** argv)
{
  QApplication app(argc, argv);

  PerlinNoiseForm form;
  form.show();

  return app.exec();
}