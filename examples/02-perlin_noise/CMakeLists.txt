cmake_minimum_required(VERSION 2.6)

set(example02_HDRS
  pnform.h
)
set(example02_SRCS
  main.cpp
  pnform.cpp
)
set(example2_UI
  mainform.ui
)

find_package(Qt4 4.5.0 COMPONENTS QtCore QtGui REQUIRED)
find_package(Lg2 REQUIRED)
find_package(Lg2Image REQUIRED)

qt4_wrap_cpp(example02_MOCS ${example02_HDRS})
qt4_wrap_ui(example02_UIS ${example2_UI})

include(${QT_USE_FILE})
include_directories(${LG2_INCLUDE_DIR} ${LG2IMAGE_INCLUDE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

add_executable(example02 ${example02_MOCS} ${example02_SRCS} ${example02_UIS})
target_link_libraries(example02
  ${QT_LIBRARIES}
  ${LG2_LIBRARIES}
  ${LG2IMAGE_LIBRARIES})
install(TARGETS example02 DESTINATION bin)