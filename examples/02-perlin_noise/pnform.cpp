#include "pnform.h"


PerlinNoiseForm::PerlinNoiseForm(QWidget* parent): QWidget(parent)
{
  ui.setupUi(this);
  ui.output->setScene(new QGraphicsScene(this));
  ui.width->setValue(256);
  ui.height->setValue(256);
  ui.depth->setValue(10);
  ui.frequenceFactor->setValue(2.0);
  ui.persistence->setValue(0.5);
  connect(ui.generateButton, SIGNAL(clicked()), this, SLOT(generate()));
}

void PerlinNoiseForm::generate()
{
  Lg::Img::PerlinNoise pn;
  pn.setDepth(ui.depth->value());
  pn.setFrequenceFactor(ui.frequenceFactor->value());
  pn.setPersistence(ui.persistence->value());
  pn.setSeed(ui.seed->value());
  switch (ui.interpolationMethod->currentIndex())
  {
    case 1:
      pn.setInterpolationMethod(Lg::Math::INTERPOLATE_COSINE);
      break;
    default:
      pn.setInterpolationMethod(Lg::Math::INTERPOLATE_LINEAR);
  }
  QImage image(ui.width->value(), ui.height->value(), QImage::Format_RGB32);
  pn.fillImage(&image);
  ui.output->scene()->clear();
  ui.output->scene()->addPixmap(QPixmap::fromImage(image));
}
