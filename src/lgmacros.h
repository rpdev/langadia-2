/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGMACROS_H
#define LGMACROS_H

#include <QtGlobal>

/** @file
  Provides some useful macros used in Langadia 2.
*/


/**
  @def LG_RESERVE_VIRTUAL_01
  @ingroup LGMacros
  This is a helper macro for binary compatibility.
  Together with the other macros LG_RESERVE_VIRTUAL_02 up to LG_RESERVE_VIRTUAL_10,
  you can use it to add reserved virtual functions to your class.

  The problem when writing classes in libraries is, that whenever you change your class
  (too much), binary compatibility breaks.

  You can use these macros to reduce the chances of that. That means:

  When creating a new class and you think, you class is stable, add the LG_RESERVE_VIRTUAL_10
  macro to your list of virtual functions.

  If it happens, that you have to add a virtual function:
  
  @li decrease the number of the macro by one (e.g. from LG_RESERVE_VIRTUAL_10 to LG_RESERVE_VIRTUAL_09)
  @li add your function right before the macro
  
 */

/**
  @def LG_RESERVE_VIRTUAL_02
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_03
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_04
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_05
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_06
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_07
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_08
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_09
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/

/**
  @def LG_RESERVE_VIRTUAL_10
  @ingroup LGMacros
  @sa LG_RESERVE_VIRTUAL_01
*/
#define LG_RESERVE_VIRTUAL_01 \
  virtual void reserved_virtual_func_01() {}
  
#define LG_RESERVE_VIRTUAL_02 \
  virtual void reserved_virtual_func_02() {}\
  LG_RESERVE_VIRTUAL_01

#define LG_RESERVE_VIRTUAL_03 \
  virtual void reserved_virtual_func_03() {}\
  LG_RESERVE_VIRTUAL_02

#define LG_RESERVE_VIRTUAL_04 \
  virtual void reserved_virtual_func_04() {}\
  LG_RESERVE_VIRTUAL_03

#define LG_RESERVE_VIRTUAL_05 \
  virtual void reserved_virtual_func_05() {}\
  LG_RESERVE_VIRTUAL_04

#define LG_RESERVE_VIRTUAL_06 \
  virtual void reserved_virtual_func_06() {}\
  LG_RESERVE_VIRTUAL_05

#define LG_RESERVE_VIRTUAL_07 \
  virtual void reserved_virtual_func_07() {}\
  LG_RESERVE_VIRTUAL_06

#define LG_RESERVE_VIRTUAL_08 \
  virtual void reserved_virtual_func_08() {}\
  LG_RESERVE_VIRTUAL_07

#define LG_RESERVE_VIRTUAL_09 \
  virtual void reserved_virtual_func_09() {}\
  LG_RESERVE_VIRTUAL_08

#define LG_RESERVE_VIRTUAL_10 \
  virtual void reserved_virtual_func_10() {}\
  LG_RESERVE_VIRTUAL_09
#endif


/**
  @def LG_EXPORT
  @ingroup LGMacros

  Macro used to flag symbols for exporting.

  Every function and class, that needs to be visible from outside, needs to
  be marked with this macro.
  It causes the function or class to be exported, when the library is compiled and
  it will let the compiler know that he must import them, when compiling a client
  application.
*/
#ifdef LANGADIA2_LIBRARY
#define LG_EXPORT Q_DECL_EXPORT
#else
#define LG_EXPORT Q_DECL_IMPORT
#endif