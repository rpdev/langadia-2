/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgperlinnoise.h"

class LgPerlinNoise::LgPerlinNoisePrivate
{

public:

    double persistence;
    double min;
    double max;
    unsigned int depth;
    double frequenceFactor;
    LgMath::InterpolationMethod interpolationMethod;
    int seed;

    double calcValue(double pos)
    {
      int i;
      double f;
      LgMath::splitDouble(pos, &i, &f);
      return LgMath::interpolate(LgMath::prandom1d(i, seed),
                               LgMath::prandom1d(i + 1, seed),
                               f, interpolationMethod);
    }

    double calcValue(double x, double y)
    {
      int ix, iy;
      double f1, f2;
      double v1, v2;
      LgMath::splitDouble(x, &ix, &f1);
      LgMath::splitDouble(y, &iy, &f2);
      v1 = LgMath::interpolate(
        LgMath::prandom2d(ix, iy, seed),
        LgMath::prandom2d(ix + 1, iy, seed),
        f1, interpolationMethod);
      v2 = LgMath::interpolate(
        LgMath::prandom2d(ix, iy + 1, seed),
        LgMath::prandom2d(ix + 1, iy + 1, seed),
        f1, interpolationMethod);
      return LgMath::interpolate(v1, v2, f2, interpolationMethod);
    }

    double calcOutputRange()
    {
      double result = 0.0,
             fact = 2.0;
      for (int i = 0; i < depth; i++)
      {
        result = result + fact;
        fact = fact * persistence;
      }
      if (result == 0.0)
      {
        result = 1.0;
      }
      return result;
    }

};

LgPerlinNoise::LgPerlinNoise( QObject* parent, double persistence, double min,
				  double max, unsigned int depth, double frequenceFactor,
                                  LgMath::InterpolationMethod interpolationMethod,
                                  int seed ) :
    QObject( parent ),
    d( new LgPerlinNoisePrivate() )
{
    d->persistence = persistence;
    d->min = min;
    d->max = max;
    d->depth = depth;
    d->frequenceFactor = frequenceFactor;
    d->interpolationMethod = interpolationMethod;
    d->seed = seed;
}

LgPerlinNoise::~LgPerlinNoise()
{
    delete d;
}

double LgPerlinNoise::perlinNoise(int x, int y)
{
  double result = 0.0,
	 freq = 0.01,
         interval = d->calcOutputRange(),
         fact = (d->max - d->min) / interval;
  for (int i = 0; i < d->depth; i++)
  {
    result = result + d->calcValue((double) x * freq, (double) y * freq) * fact;
    freq = freq * d->frequenceFactor;
    fact = fact * d->persistence;
  }
  return result + d->min + (d->max - d->min) * 0.5;
}

double LgPerlinNoise::perlinNoise(int p)
{
  double result = 0.0,
	 freq = 0.01,
         interval = d->calcOutputRange(),
         fact = (d->max - d->min) / interval;
  for (int i = 0; i < d->depth; i++)
  {
    result = result + d->calcValue((double) p * freq) * fact;
    freq = freq * d->frequenceFactor;
    fact = fact * d->persistence;
  }
  return result + d->min + (d->max - d->min) * 0.5;
}

#include <stdio.h>
void LgPerlinNoise::fillImage(QImage* image)
{
  if (image == NULL)
  {
    return;
  }
  for (int x = 0; x < image->width(); x++)
  {
    for (int y = 0; y < image->height(); y++)
    {
      int val = LgMath::fitRange(LgMath::iPart(perlinNoise(x, y)), 0, 255);
      image->setPixel(x, y, qRgb(val, val, val));
    }
  }
}

double LgPerlinNoise::persistence() const
{
    return d->persistence;
}

void LgPerlinNoise::setPersistence( double persistence )
{
    d->persistence = persistence;
}

double LgPerlinNoise::min() const
{
    return d->min;
}

void LgPerlinNoise::setMin( double min )
{
    d->min = min;
}

double LgPerlinNoise::max() const
{
    return d->max;
}

void LgPerlinNoise::setMax( double max )
{
    d->max = max;
}

unsigned int LgPerlinNoise::depth() const
{
    return d->depth;
}

void LgPerlinNoise::setDepth( unsigned int depth )
{
    d->depth = depth;
}

double LgPerlinNoise::frequenceFactor() const
{
    return d->frequenceFactor;
}

void LgPerlinNoise::setFrequenceFactor( double frequenceFactor )
{
    d->frequenceFactor = frequenceFactor;
}

LgMath::InterpolationMethod LgPerlinNoise::interpolationMethod() const
{
    return d->interpolationMethod;
}

void LgPerlinNoise::setInterpolationMethod( LgMath::InterpolationMethod interpolationMethod )
{
    d->interpolationMethod = interpolationMethod;
}

int LgPerlinNoise::seed() const
{
    return d->seed;
}

void LgPerlinNoise::setSeed( int seed )
{
    d->seed = seed;
}
