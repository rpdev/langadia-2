/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGPERLINNOISE_H
#define LGPERLINNOISE_H

/** @file
  Provides perlin noise generator.
*/

#include "../lgmacros.h"
#include "../core/lgmath.h"

#include <QObject>
#include <QImage>


/**
  * @brief Perlin noise generator.
  *
  * The PerlinNoise class generator pseudo-random content, which
  * is equal for same input arguments but looks rather random and varies by
  * even slightly changing parameters.
  * The produced content can be used for several things as textures, heightmaps,
  * cloud structures and so on.
 */
class LG_EXPORT LgPerlinNoise : public QObject
{

    Q_OBJECT

    /**
      * @brief The persistence parameter for the perlin noise algorithm.
      *
      * The persistence determines the influence of higher frequencies.
      */
    Q_PROPERTY(double persistence READ persistence WRITE setPersistence)

    /**
      * @brief Determines the lower limit for output values.
      *
      * Output will not be less than min.
      */
    Q_PROPERTY(double min READ min WRITE setMin)

    /**
      * @brief Determines the upper limit for output values.
      *
      * Output will not be more than max.
      */
    Q_PROPERTY(double max READ max WRITE setMax)

    /**
      * @brief The depth parameter for the perlin noise algorithm.
      *
      * The depth determines the number of iterations.
      */
    Q_PROPERTY(unsigned int depth READ depth WRITE setDepth)

    /**
      * @brief The frequence factor.
      *
      * The frequence  will be multiplied with
      * the frequence factor in each iteration.
      */
    Q_PROPERTY(double frequenceFactor READ frequenceFactor WRITE setFrequenceFactor)

    /**
      * @brief Seed for pseudo random number generation.
      *
      * Usually, increasion
      * this value by e.g. one heavily changes the output. However, for same
      * seeds the output will be the same.
      */
    Q_PROPERTY(int seed READ seed WRITE setSeed)

public:

    /**
       Constructor.
     */
    LgPerlinNoise(QObject* parent = 0,
                  double persistence = 0.5,
                  double min = 0.0,
                  double max = 255.0,
                  unsigned int depth = 10,
                  double frequenceFactor = 2.0,
                  LgMath::InterpolationMethod interpolationMethod = LgMath::INTERPOLATE_LINEAR,
                  int seed = 0);

    /**
      * @brief Destructor.
      */
    virtual ~LgPerlinNoise();

    /**
      * @brief Calculates noise in 2D.
      *
      * The method takes a position in 2D as input, given through @p x and @p y.
      * The output is a random value, however, depending on the set up members
      * of the class, between two neighboured positions the output will not exceed a
      * certain treshold.
      */
    Q_INVOKABLE double perlinNoise( int x, int y );

    /**
      * @brief Calculates noise in 1D.
      *
      * The method takes a position in 1D as input, given through @p p.
      * The output is a random value, however, depending on the set up members
      * of the class, between two neighboured positions the output will not exceed a
      * certain treshold.
      */
    Q_INVOKABLE double perlinNoise( int p );

    /**
      * @brief Fill the given image with perlin noise.
      *
      * This method fills the @p image with perlin noise. This method
      * obeys the set up properties (especially min and max). If you want to e.g.
      * get values between 0 and 255, you have to set min to 0 and max to 255.
      */
    Q_INVOKABLE void fillImage( QImage * image );

    /**
      * @brief Returns the persistence.
      */
    double persistence() const;

    /**
      * @brief Sets the persistence.
      */
    void setPersistence(double persistence);

    /**
      * @brief Returns the lower limit for output values.
      */
    double min() const;

    /**
      * @brief Sets the lower limit for output values.
      */
    void setMin(double min);

    /**
      * @brief Returns the upper limit for output values.
      */
    double max() const;

    /**
      * @brief Sets the upper limit for output values.
      */
    void setMax(double max);

    /**
      * @brief Returns the number of iterations of the algorithm.
      */
    unsigned int depth() const;

    /**
      * @brief Sets the number of iterations of the algorithm.
      */
    void setDepth(unsigned int depth);

    /**
      * @brief Returns the frequence factor.
      */
    double frequenceFactor() const;

    /**
      * @brief Sets the frequence factor.
      */
    void setFrequenceFactor(double frequenceFactor);

    /**
      * @brief Returns the used interpolation method.
      */
    LgMath::InterpolationMethod interpolationMethod() const;

    /**
      * @brief Sets the interpolation method to use.
      */
    void setInterpolationMethod(LgMath::InterpolationMethod interpolationMethod);

    /**
      * @brief Returns the seed used for pseudo random number generation.
      */
    int seed() const;

    /**
      * @brief Sets the seed used for pseudo random number generation.
      */
    void setSeed(int seed);

private:

    class LgPerlinNoisePrivate;
    LgPerlinNoisePrivate* d;

};

#endif // LGPERLINNOISE_H
