/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgmath.h"

#include "math.h"

double LgMath::prandom1d(int x, int seed)
{
  x = x + seed;
  x = (x << 13) xor x;
  return 1 - ((x*(x*x*12731+789221)+1376312589) & 0x7fffffff)/1073741824*2;
}

double LgMath::prandom2d(int x, int y, int seed)
{
  int n = x + y * 57 + seed;
  n = (n << 13) xor n;
  return prandom1d(n, seed * seed + seed);
}

double LgMath::interpolateLinear(double v1, double v2, double mv1)
{
    if (mv1 < 0.0)
    {
        mv1 = 0.0;
    }
    if (mv1 > 1.0)
    {
        mv1 = 1.0;
    }
    return v2 * mv1 + v1 * (1.0 - mv1);
}

double LgMath::interpolateCosine(double v1, double v2, double mv1)
{
    if (mv1 < 0.0)
    {
        mv1 = 0.0;
    }
    if (mv1 > 1.0)
    {
        mv1 = 1.0;
    }
    double f = (cos(M_PI*mv1)+1.0)/2.0;
    return v1 * f + v2 * (1 - f);
}

double LgMath::interpolate(double v1, double v2, double mv1, LgMath::InterpolationMethod method)
{
  switch (method)
  {
    case INTERPOLATE_COSINE:
      return interpolateCosine(v1, v2, mv1);
    default:
      return interpolateLinear(v1, v2, mv1);
  }
}

void LgMath::splitDouble(double val, int* i, double* d)
{
  int ip = trunc(val);
  double dp = val - (double) ip;
  if (i != NULL)
  {
    *i = ip;
  }
  if (d != NULL)
  {
    *d = dp;
  }
}

int LgMath::iPart(double val)
{
  int i = 0;
  splitDouble(val, &i, NULL);
  return i;
}

int LgMath::fPart(double val)
{
  double f = 0.0;
  splitDouble(val, NULL, &f);
  return f;
}

int LgMath::fitRange(int val, int min, int max)
{
  if (val < min)
  {
    val = min;
  }
  if (val > max)
  {
    val = max;
  }
  return val;
}


