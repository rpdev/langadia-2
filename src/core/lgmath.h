/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGMATH_H
#define LGMATH_H

/** @file
  Provides some mathematical functionality.
*/
#include "../lgmacros.h"


/**
  * @brief Class providing mathematical functions.
  *
  *  The Math class hosts the mathmatical functions. This class
  *  cannot be instantiated. It rather provides static methods one
  *  can use.
  */
class LG_EXPORT LgMath
{
public:

    /**
      * @brief Determine interpolation method used for some methods which uses
      *        switches to dynamically change the used method.
      */
    enum InterpolationMethod
    {
	INTERPOLATE_LINEAR = 0, ///< Use linear interpolation. Faster but harder edges.
	INTERPOLATE_COSINE = 1  ///< Use cosine interpolation. Smoother results but slower.
    };

public:

    /**
      * @brief Generate preudo random numbers.
      *
      * Creates a pseudo random number from given input.
      * This functions returns the same value for equal input.
      *
      * @param x A 1D position.
      * @param seed Random seed.
      *
      * @return A value between -1 and +1.
      */
    static double prandom1d(int x, int seed = 0);

    /**
      * @brief Generate pseudo random numbers.
      *
      * Creates a pseudo random number from given input.
      * This funtion returns the same value for equal input.
      * This function takes a 2D position (@p x | @p y) and
      * generates a "random" number.
      * You can get varying output by passing other values for
      * the random @p seed.
      *
      * @return A value between -1 and +1.
      */
    static double prandom2d(int x, int y, int seed = 0);

    /**
      * @brief Interpolate between two values.
      *
      * This function interpolates between @p v1 and @p v2, depending
      * on the value of @p mv1. If mv1 is zero, this function returns v1,
      * if mv1 is 1, v2 is returned. Every other value returns a value
      * between v1 and v2.
      * This functions uses linear interpolation, which is quit fast,
      * but produces unaccurate output in some cases.
      *
      * @sa interpolateCosine()
      * @sa interpolate()
      */
    static double interpolateLinear(double v1, double v2, double mv1);

    /**
      * @brief Interpolate between two values.
      *
      * This function interpolates between @p v1 and @p v2, depending
      * on the value of @p mv1. If mv1 is zero, this function returns v1,
      * if mv1 is 1, v2 is returned. Every other value returns a value
      * between v1 and v2.
      * This function uses sinusial interpolation, which produces pretty good
      * output, but can be somewhat slow.
      *
      * @sa interpolateLinear()
      * @sa interpolate()
      */
    static double interpolateCosine(double v1, double v2, double mv1);

    /**
      * @brief Interpolate between two values.
      *
      * This function interpolates between @p v1 and @p v2, depending
      * on the value of @p mv1. If mv1 is zero, this function returns v1,
      * if mv1 is 1, v2 is returned. Every other value returns a value
      * between v1 and v2.
      * The interpolation method used is determines by @p method. The default
      * method is linear interpolation.
      *
      * @sa interpolateCosine()
      * @sa interpolateLinear()
      */
    static double interpolate(double v1, double v2, double mv1,
                              InterpolationMethod method = INTERPOLATE_LINEAR);

    /**
      * @brief Seperates the integral and the float part of a double value.
      *
      * @param val The value to separate.
      * @param i An integer where to store the integer part in.
      * @param d A double where to store the float part in.
      */
    static void splitDouble(double val, int * i, double * d);

    /**
      * @brief Returns the integer part of a floating point value.
      *
      * Returns the integer part of @p val.
      */
    static int iPart(double val);

    /**
      * @brief Returns the float part of a floating point value.
      *
      * Returns the float part of @p val.
      */
    static int fPart(double val);

    /**
      * @brief Fit a value into a given range.
      *
      * Returns @p val, if min <= val <= max.
      * Returns @p min, if val < min.
      * Returns @p max, if val > max.
      */
    static int fitRange(int val, int min, int max);

private:

    LgMath() {}
    LgMath(LgMath &c) {}

};

#endif // LGMATH_H
