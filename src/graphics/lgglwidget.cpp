/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgglwidget.h"


class LgWidget::WidgetPrivate
{
};






LgWidget::LgWidget(QWidget* parent, const QGLWidget* shareWidget, Qt::WindowFlags f):
  QGLWidget(parent, shareWidget, f), m_Timer()
{
  d = new WidgetPrivate();
  
  connect(&m_Timer, SIGNAL(timeout()), this, SLOT(update()));
  setAutoRedraw(false);
  setAutoRedrawInterval(0);
  
  setAttribute(Qt::WA_NoSystemBackground);

  m_Time.start();
  m_LastDraw2D = m_Time.elapsed();
  m_LastDraw3D = m_Time.elapsed();
  m_View = NULL;
}

LgWidget::~LgWidget()
{
  delete d;
}

void LgWidget::initializeGL()
{
  emit initialize3D();
}

void LgWidget::resizeGL(int width, int height)
{
  emit resize3D(width, height);
}

void LgWidget::paintGL()
{
  int curTime = m_Time.elapsed();
  emit paint3D(curTime - m_LastDraw3D);
  m_LastDraw3D = curTime;
}

void LgWidget::paintEvent(QPaintEvent* event)
{
  Q_UNUSED(event);
  
  QPainter painter;

  painter.begin(this);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glPushAttrib(GL_ALL_ATTRIB_BITS);

  initializeGL();
  if (m_View != NULL)
  {
    m_View->saveCurrentView();
    QRect r(0, 0, width(), height());
    m_View->activate(&r);
  }
  
  resizeGL(width(), height());
    
  paintGL();

  if (m_View != NULL)
  {
    m_View->restoreCurrentView();
  }

  glPopAttrib();

  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  int curTime = m_Time.elapsed();
  emit paint2D(&painter, curTime - m_LastDraw2D);
  m_LastDraw2D = curTime;

  painter.end();
}

void LgWidget::setAutoRedraw(bool redraw)
{
  if (redraw)
  {
    m_Timer.start();
  }
  else
  {
    m_Timer.stop();
  }
}

void LgWidget::setAutoRedrawInterval(int interval)
{
  m_Timer.setInterval(interval);
}

void LgWidget::setView(LgView* view)
{
  m_View = view;
}


