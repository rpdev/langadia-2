/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGANIMATION_H
#define LGANIMATION_H

/** @file
Provides animation. This file contains the Animation class, which can be used to
display an animation, which is internally a list of tiles, each assigned a duration
it is displayed. 
*/

#include <QObject>
#include <QTime>
#include <QList>

#include "lganimationframe.h"


/**
      Class for displaying animations. An animation is - basically - a sequence of
      several tiles. Each tile is encapsulated by an AnimationFrame, which bears additional
      information, as the duration for each tile. 
    */
class LG_EXPORT LgAnimation : public QObject
{

    Q_OBJECT

    /**
 Advance automatically.
 Set this to true to automatically advance the animation. Otherwise, you
 have to advance the animation manually, which - however - gives you more
 possibilities (e.g. slow-motion animation and so forth).
      */
    Q_PROPERTY(bool autoAdvance READ autoAdvance WRITE setAutoAdvance)

    /**
 The frames of the animation. Each frame contains a pointer to a tile,
 a duration and a rectangle to indicate, which part of the tile to draw
 (which allows multiple frames inside one big tile.
      */
    Q_PROPERTY(QList<LgAnimationFrame*>* frames READ frames)

    /**
 The current progress of the animation. This value is between 0 and length() and indicates
 the current progress (and with it the currently show tile) of the animation.
      */
    Q_PROPERTY(int progress READ progress WRITE setProgress)

    /**
 The length of the animation in milliseconds. The length depends on the AnimationFrame's
 added to the animation. This property is read-only.
 @sa frames
      */
    Q_PROPERTY(int length READ length)


public:

    /*
   Constructor and Destructor
 */
    /**
   The constructor. @p autoAdv indicates, whether the animation should advance automatically.
 */
    LgAnimation(QObject* parent = NULL, bool autoAdv = true);

    /**
   Destructor.
 */
    virtual ~LgAnimation();



    /*
   Methods
 */

    /**
   Advance the animation.
   Increases the progress by @p time, where time is a number of milliseconds.
 */
    void advance(int time) { setProgress(progress() + time); }



    /*
   Getter and Setter
 */

    /**
   Getter for autoAdvance property.
 */
    bool autoAdvance() { return m_AutoAdvance; }

    /**
   Setter for autoAdvance property. This will set autoAdvance to @p aa.
 */
    void setAutoAdvance(bool aa);

    /**
   Getter for frames property.
 */
    QList<LgAnimationFrame*>* frames() { return &m_Frames; }

    /**
   Getter for the progress property. Returns the progress of the animation in milliseconds.
 */
    int progress();

    /**
   Setter for the progress property. Sets the progress of the animation in milliseconds.
   The value passed will be changes to fit in the range between 0 up to length().
 */
    void setProgress(int progress);

    /**
   The total length of the animation in milliseconds.
 */
    int length();

    /**
   Returns the currently shown frame of the animation.
 */
    LgAnimationFrame* currentFrame();

    /**
   Draw the animation. This is an overloaded version.
 */
    void draw(QPoint position, QColor color);

    /**
   Draw the animation. This is an overloaded version.
 */
    void draw(QPoint position);

    /**
   Draw the animation. The animation is drawn at position (@p x|@p y). It is blended using @p color.
 */
    void draw(int x, int y, QColor color);

    /**
   Draw the animation. This is an overloaded version.
 */
    void draw(int x, int y);

private:

    class AnimationPrivate;
    AnimationPrivate* d;

    bool m_AutoAdvance;
    QTime m_Time;
    int m_LastAutoUpdateTime;
    QList<LgAnimationFrame*> m_Frames;
    int m_Progress;
};


#endif
