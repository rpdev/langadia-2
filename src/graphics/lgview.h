/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGVIEW_H
#define LGVIEW_H

/** @file
  Provides base class for views. A view defines, how
  the scene is presented. Basically, there are orthogonal and
  perspective views. 
*/

#include <QObject>
#include <QtOpenGL>

#include "../lgmacros.h"


class LgWidget;

/**
      Base class for views. Provides some basic methods. However, its main purpose is to
      provide an abstract base for concrete view classes.
    */
class LG_EXPORT LgView : public QObject
{

    Q_OBJECT

public:

    /**
   Constructor.
 */
    LgView(QObject* parent = NULL);

    /**
   Destructor.
 */
    virtual ~LgView();



    /**
   Save the current view. This will push the projection and modelview matrix onto
   OpenGL's stack. Calling this will set the modelview matrix as the active one.
   Also note, that every call to this method must have an associated call to
   restoreCurrentView().

 */
    void saveCurrentView();

    /**
   Restore the last active view. To call this, a call to saveCurrentView() must have
   been made before.
 */
    void restoreCurrentView();

    /**
          Activate the view. Sets the view for the active OpenGL context. You can pass the
          dimension of the underlying widget using @p rect.
 */
    virtual void activate(QRect* rect = 0) = 0;

private:

    class ViewPrivate;
    ViewPrivate* d;

};

#endif
