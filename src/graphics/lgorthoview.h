/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGORTHOVIEW_H
#define LGORTHOVIEW_H

/** @file
  Provides orthogonal view.
*/

#include <QObject>
#include <QRect>
#include <QtOpenGL>

#include "lgview.h"


/**
      Orthogonal view mode. Provides a View implementation for setting
      up orthogonal views. This is useful for drawing 2D stuff.
      The OrthoView class usually uses a rectangle, determining the coordinates of the lower
      left corner as well as (virtual) width and height of the scene.
      One can also pass a rectangle with zero width and/or height.
      This will use the width and height of the device, that can be passed to the
      activate() method: The lower left corner will get coordinates (0|0), the upper
      right one (width|height). 
    */
class LG_EXPORT LgOrthoView : public LgView
{

    Q_OBJECT

    /**
 The size and offset of the virtual screen. The lower left corner coordinates
 are determined by the x and y properties of the rect. Width and height equal to
 the size of rect.
      */
    Q_PROPERTY(QRect rect READ rect WRITE setRect)

    /**
 The distance of the near clipping plane. Every fragment, whose z-coordinate is below
 this will be clipped.
      */
    Q_PROPERTY(qreal near READ near WRITE setNear)

    /**
 The distance of the far clipping plane. Every fragment, whose z-coordinate is above this
 value will be clipped.
      */
    Q_PROPERTY(qreal far READ far WRITE setFar)

public:

    /**
   Constructor.
   @p screen is the rectangle determining the virtual coordinates of the screen.
   @p near and @p far determine the front and back clipping planes.
 */
    LgOrthoView(QObject* parent = NULL, QRect screen = QRect(0, 0, 0, 0), qreal near = 0.0, qreal far = 100.0);

    /**
   Destructor.
 */
    virtual ~LgOrthoView();

    /*
   Implementation of Lg::Gfx::View::activate()
 */
    void activate(QRect* rect = 0);


    /**
   Getter for the rect property.
 */
    QRect rect() { return m_Rect; }

    /**
   Setter for the rect property.
 */
    void setRect(QRect rect);

    /**
   Getter for the near property.
 */
    qreal near() { return m_Near; }

    /**
   Setter for the near property.
 */
    void setNear(qreal near);

    /**
   Getter for the far property.
 */
    qreal far() { return m_Far; }

    /**
   Setter for the far property.
 */
    void setFar(qreal far);

private:

    class OrthoViewPrivate;
    OrthoViewPrivate* d;

    QRect m_Rect;
    qreal m_Near;
    qreal m_Far;
};

#endif
