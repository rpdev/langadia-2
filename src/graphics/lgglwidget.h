/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGGLWIDGET_H
#define LGGLWIDGET_H

/** @file
  Provides the OpenGL widget, on which hardware accelerated content is drawn.
*/

#include <QObject>
#include <QGLWidget>
#include <QPainter>
#include <QTimer>
#include <QTime>

#include "../lgmacros.h"
#include "lgview.h"


/**
      Widget class for displaying OpenGL content.
      This class provides a widget, onto which OpenGL (hardware accelerated)
      content can be drawn.
     */
class LG_EXPORT LgWidget : public QGLWidget
{

    Q_OBJECT

    /**
 Auto redrawing of the widget.
 If autoRedraw is set to true, the widget uses an timer to periodically
 update its contents.

 You can set autoRedrawInterval, to determine the time between redrawings.
      */
    Q_PROPERTY(bool autoRedraw READ autoRedraw WRITE setAutoRedraw)

    /**
 The interval between auto redrawings.
      */
    Q_PROPERTY(int autoRedrawInterval READ autoRedrawInterval WRITE setAutoRedrawInterval)

    /**
 The view used for this widget. Set this, if you want to automatically
 set the view for 3D drawing. This will not cause to emit the resize3D signal.
      */
    Q_PROPERTY(LgView* view READ view WRITE setView)

public:

    // static methods & constructors & destructors

    /**
   Constructor.
   Called to create a new Widget object. @p parent is the parent widget, to which
   this will be added. @p shareWidget is used to share data between the OpenGL Rendering
   Contexts of two (or more) widgets. This is only possible, if both
   widgets use the same formats.
   @p f are some window flags, that can be passed to the widget.
 */
    LgWidget(QWidget* parent = 0, const QGLWidget* shareWidget = 0, Qt::WindowFlags f = 0);

    /**
   Destructor.
 */
    virtual ~LgWidget();

    /*
   These members override functions in QGLWidget.
 */

    /**
   Initialize 3D drawing.
   This is called to initialize 3D drawing. This method is called once per render pass
   to initialize OpenGL for drawing. It emits the initialize3D signal.
 */
    void initializeGL();

    /**
   Resize OpenGL viewport.
   This is called once every render pass before the widget is drawn. It allows you to
   adjust the OpenGL viewport and projection matrix to the widgets current size. The widgets
   dimension is passed in through @p width and @p height.
   This method emits the resize3D signal.
 */
    void resizeGL(int width, int height);

    /**
   Paint 3D content.
   This is called to draw the 3D contents once every render pass.
   The methods emits the paint3D signal.
 */
    void paintGL();

    /**
   Update the widget.
   This is called when the widget is redrawn. This method initializes an
   QPainter, which is later used to draw plain 2D stuff (so called overdrawing).
   Before overdrawing, it calls the methods for setting up 3D drawing and
   actually calls the paintGL method to display a 3D scene.
   It then resets view and state and emits the paint2D signal to let
   you draw 2D stuff onto the widget.
 */
    void paintEvent(QPaintEvent* event);


    // Getter and Setter

    /**
   Automatic redrawing of widget.
   If autoRedraw is true, the widget uses an internal timer to periodically
   redraw its contents. This is usually required when drawing OpenGL contens.
   By default, auto redrawing is turned off.
   @sa autoRedrawInterval
 */
    bool autoRedraw() const { return m_Timer.isActive(); }

    /**
   Turn auto redrawing on or off.
   This method sets the auto redrawing mode of the widget to @p redraw. See also autoRedraw() for
   a more detailed description.
   @sa autoRedrawInterval
 */
    void setAutoRedraw(bool redraw);

    /**
   Interval between automatic redraws.
   This returns the time in milliseconds between two paint3D/paint2D signals.
   @sa autoRedraw
 */
    int autoRedrawInterval() const { return m_Timer.interval(); }

    /**
   Set the auto redraw interval.
   This sets the interval between to paint3D/paint2D signals to @p interval.
   @sa autoRedraw
 */
    void setAutoRedrawInterval(int interval);

    /**
   Getter for the view property. Returns the currently set view for the widget.
 */
    LgView* view() { return m_View; }

    /**
   Setter for the view property. Sets the view to use with this
   widget to @p view.
 */
    void setView(LgView* view);


    // virtual methods

    LG_RESERVE_VIRTUAL_10


    signals:

	/**
       Initialize 3D drawing.
       This signal is emitted just before 3D drawing begins. It can be used to do one-time
       initialization (per render-loop).
     */
	void initialize3D();

    /**
   Resize view of the widget for 3D drawing.
   This is emitted just before 3D drawing. You can use it to set the viewport to the current
   widgets dimension, which is passed in through the parameters @p width and @p height.
 */
    void resize3D(int width, int height);

    /**
   Paint 3D scene.
   This is called to draw the 3D scene. Use it to paint the 3D stuff you want to display.
   Usually, the context should already been initialized, as initialize3D and resize3D have already been
   emitted. However, you can also perform initialization and resizing here.
   The @p advance parameter indicates the time since the last paint3D signal in milliseconds.
 */
    void paint3D(int advance);

    /**
   Paint 2D scene.
   Connect to this signal to draw 2D stuff onto the widget. Drawing should happen
   using the @p painter parameter. Note, that initialization and resizing has been done internally
   by the painter! If you want to draw using OpenGL directly, rather use the paint3D signal.
   There, you also can switch to 2D "view" (see glOrtho function) and perform drawing.
   The @p advance parameter indicates the time since the last paint2D signal.
 */
    void paint2D(QPainter* painter, int advance);


private: // Members

    class WidgetPrivate;
    WidgetPrivate* d;

    QTimer m_Timer;
    QTime m_Time;
    int m_LastDraw3D;
    int m_LastDraw2D;
    LgView* m_View;
};

#endif
