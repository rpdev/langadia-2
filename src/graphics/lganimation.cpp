/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lganimation.h"

class LgAnimation::AnimationPrivate
{
};




LgAnimation::LgAnimation (QObject* parent, bool autoAdv ) : QObject (parent)
{
  d = new AnimationPrivate;
  
  m_AutoAdvance = autoAdv;
  m_Time.start();
}

LgAnimation::~LgAnimation()
{
  delete d;
}

void LgAnimation::setAutoAdvance ( bool aa )
{
  m_AutoAdvance = aa;
}

int LgAnimation::progress()
{
  return m_Progress;
}

void LgAnimation::setProgress(int progress)
{
  if (length() > 0)
  {
    while (progress < 0)
    {
      progress += length();
    }
    progress = progress % length();
  }
  else
  {
    progress = 0;
  }
  m_Progress = progress;
}

int LgAnimation::length()
{
  int result = 0;
  for (int i = 0; i < m_Frames.count(); i++)
  {
    result += m_Frames[i]->duration();
  }
  return result;
}

LgAnimationFrame* LgAnimation::currentFrame()
{
  int curBegin = 0;
  for (int i = 0; i < m_Frames.count(); i++)
  {
    if (progress() >= curBegin && progress() < curBegin + m_Frames[i]->duration())
    {
      return m_Frames[i];
    }
    curBegin += m_Frames[i]->duration();
  }
  return NULL;
}

void LgAnimation::draw(QPoint position, QColor color)
{
  draw(position.x(), position.y(), color);
}

void LgAnimation::draw(QPoint position)
{
  draw(position, Qt::white);
}

void LgAnimation::draw(int x, int y, QColor color)
{
  if (m_AutoAdvance)
  {
    int curTime = m_Time.elapsed();
    int elapsed = curTime - m_LastAutoUpdateTime;
    advance(elapsed);
    m_LastAutoUpdateTime = curTime;
    
  }
  LgAnimationFrame* curFrame = currentFrame();
  if (curFrame != NULL)
  {
    curFrame->tile()->draw(QRect(x, y, curFrame->rect().width(), curFrame->rect().height()),
			   color, curFrame->rect());
  }
}

void LgAnimation::draw(int x, int y)
{
  draw(x, y, Qt::white);
}
