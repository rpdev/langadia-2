/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGTEXTURE_H
#define LGTEXTURE_H

/**
  @file
  Provides Texture class.
  This file contains a class for managing textures. This class wraps around
  the OpenGL texture object.
*/
#include <QObject>
#include "lgglwidget.h"

/**
      Texture management class.
      Texture is a class that wraps an OpenGL texture object. OpenGL textures
      are numbers, identifying an OpenGL object. This class encapsulates exactly one
      such object and provides some operations upon it.
    */
class LG_EXPORT LgTexture : public QObject
{

    Q_OBJECT

    /**
 The OpenGL texture object the Texture object wraps around. Access this if you need
 to use OpenGL functions directly.
      */
    Q_PROPERTY(GLuint texture READ texture WRITE setTexture)

    /**
 The policy how to act on release. If this is true, the OpenGL texture object
 will be deleted when the texture object is deleted. If it is false, the texture
 will survive, what is what you want when multiple Texture objects are sharing the
 same OpenGL texture.
      */
    Q_PROPERTY(bool deleteOnRelease READ deleteOnRelease WRITE setDeleteOnRelease)

    /**
 The width of the texture. This value is the number of pixels in each row of the texture.
      */
    Q_PROPERTY(int width READ width)

    /**
 The height of the texture. This value indicates the number of pixels per column.
      */
    Q_PROPERTY(int height READ height)



public:

    /**
   Constructor.
   Called to create a new texture object. @p textureId is the OpenGL texture object,
   to wrap. @p delOnRelease determines the management policy for this object. By default,
   true is passed, which indicates, that the texture object shall delete the OpenGL
   texture as soon as itself is destroyed.
   Pass false, if you want the OpenGL texture to survive the release. This is
   probably what you want if more than one Texture objects are sharing one OpenGL texture.
 */
    LgTexture(QObject* parent = 0, GLuint textureId = 0, bool delOnRelease = true);

    /**
   Called to release the Texture.
   This performs some internal cleanup and deletes the texture finally.
 */
    virtual ~LgTexture();

    /**
   Activate the texture object.
   Binds the texture object and makes it the active one.
 */
    void bindTexture();

    /**
   Delete the texture.
   This releases the internal texture data. This method does not care about
   the deleteOnRelease value.
 */
    void releaseTexture();


    /*
   Getter and Setter
 */

    /**
   Getter for the texture. Returns the OpenGL texture id.
 */
    GLuint texture() { return m_Texture; }

    /**
   Setter for the texture. Sets the OpenGL texture id. If deleteOnRelease is true,
   the old texture will be released.
 */
    void setTexture(GLuint texId);

    /**
   Getter for deleteOnRelease. The value returned indicates policy on delete.
 */
    bool deleteOnRelease() { return m_DeleteOnRelease; }

    /**
   Setter for deleteOnRelease. If @p delOnRelease is true, the wrapped texture object is
   automatically deleted then the object dies. If not, the texture will survive, which is what
   you want then multiple Texture objects share the same OpenGL texture.
 */
    void setDeleteOnRelease(bool delOnRelease);

    /**
   The texture's width.
   Returns the width of the texture in pixels.
 */
    int width() { return m_Width; }

    /**
   The texture's height.
   Returns the height of the texture in pixels.
 */
    int height() { return m_Height; }



private:

    class TexturePrivate;
    TexturePrivate* d;
    GLuint m_Texture;
    GLint m_Width;
    GLint m_Height;
    bool m_DeleteOnRelease;
};

#endif
