/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGGRAPHICSVIEW_H
#define LGGRAPHICSVIEW_H

/** @file
  Provides a QGraphicsView subclass.
  */
#include <QGraphicsView>
#include <QGLWidget>
#include <QResizeEvent>
#include "../lgmacros.h"


/**
      GraphicsView subclasses the QGraphicsView class to provide
      views onto which both OpenGL accelerated content and
      Qt widgets can be displayed. It internally works with an
      QGLWidget, onto which everything is drawn.To display widgets,
      it works together with the GraphicsScene class.
      */
class LG_EXPORT LgGraphicsView : public QGraphicsView
{

    Q_OBJECT

public:

    /*
          Constructor & Destructor
          */

    /**
          Constructor.
          Called to create a new GraphicsView. @p parent is the widgets parent.
          If you want the view to create a widget to draw on itself, pass true for the
          @p initializeGLWidget argument. You can pass @p format to specify the concrete
          pixel format to use for that auto-created QGLWidget.
          */
    LgGraphicsView(QWidget* parent = 0, bool initializeGLWidget = true,
                 QGLFormat format = QGLFormat(QGL::SampleBuffers));

    /**
          Destructor.
          */
    virtual ~LgGraphicsView();

    /*
          Methods
          */

    /*
          Getter & Setter
          */

protected:

    /*
          Event Handler
          */

    /**
          Called, when the view is resized.
          */
    void resizeEvent(QResizeEvent* event);

private:

    class GraphicsViewPrivate;
    GraphicsViewPrivate* d;

};


#endif
