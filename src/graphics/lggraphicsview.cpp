/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lggraphicsview.h"
#include <QRect>
#include <QPoint>

class LgGraphicsView::GraphicsViewPrivate
{
};


LgGraphicsView::LgGraphicsView(QWidget* parent, bool initializeGLWidget, QGLFormat format) :
        QGraphicsView(parent)
{
    d = new GraphicsViewPrivate();
    if (initializeGLWidget)
    {
        setViewport(new QGLWidget(format));
    }
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
}

LgGraphicsView::~LgGraphicsView()
{
    delete d;
}

void LgGraphicsView::resizeEvent(QResizeEvent* event)
{
    if (scene() != NULL)
    {
        scene()->setSceneRect(QRect(QPoint(0, 0), event->size()));
    }
    QGraphicsView::resizeEvent(event);
}
