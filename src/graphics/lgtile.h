/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGTILE_H
#define LGTILE_H

#include <QObject>
#include <QRect>

#include "../lgmacros.h"
#include "lgtexture.h"

/**
      Class for drawing 2D images.
      The Tile class can be used to draw 2D images (so called tiles) onto an
      OpenGL accelerated widget.
      This class is based upon the Texture class. Indeed, they are nearly the
      same except that the Tile class provides further methods for drawing, while
      Texture is just a wrapper for the OpenGL texture object.
     */
class LG_EXPORT LgTile : public LgTexture
{

    Q_OBJECT

public:

    /*
     Constructors and Destructors
   */

    /**
     The construction. Creates a new tile, wrapping the OpenGL texture passed
     by @p textureId. If @p delOnRelease is true, the texture deletes the OpenGL texture
     when it is released.
   */
    LgTile(QObject* parent = NULL, GLuint textureId = 0, bool delOnRelease = true);

    /**
     Destructor.
   */
    virtual ~LgTile();

    /*
     Methods
   */

    /**
     Draw the tile.
     The tile's lower left corner will be located at rect.x() and rect.y(). Its width and
     height correspond with the dimension of @p rect, too.
     The tile will be blended with @p color.
     @p clip can be used to only display a part of the underlying texture; e.g. passing
     QRect(0, 0, 10, 10) would only display the lower left square of dimension 10x10.
   */
    void draw(QRect rect, QColor color, QRect clip);

    /**
     Draw the tile.
     This is an overloaded version of draw. It passes @p rect and @p color and displays
     the full texture.
   */
    void draw(QRect rect, QColor color);

    /**
     Draw the tile.
     This is an overloaded version of draw. It passes @p rect and uses white as color and
     displays the full texture.
   */
    void draw(QRect rect);

    /**
     Draw the tile.
     This is an overloaded version. It displays the tile at (@p x/ @p y), using the texture dimension
     for width and height. It blends the tile using @p color and displays the part of the
       texture determined by @p clip.
   */
    void draw(int x, int y, QColor color, QRect clip);

    /**
     Draw the tile.
     This is an oberloaded version.
     It passes @p x and @p y, using @p color for blending and displays the full texture.
   */
    void draw(int x, int y, QColor color);

    /**
     Draw the tile.
     This is an overloaded version.
     It passes @p x and @p y, using white for blending and displays the full texture.
   */
    void draw(int x, int y);


private:

    class TilePrivate;
    TilePrivate* d;

};

#endif
