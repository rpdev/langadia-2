/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGOBJECT3D_H
#define LGOBJECT3D_h

/** @file
  This file contains the Object3D class, the base for all object, that
  can be positioned in 3D space.
  */

#include <QObject>
#include "../lgmacros.h"


/**
      Base class for all objects that can be positioned on 3D space.
      The Object3D class prodides a base class for all entities, that can be positioned
      in 3D space. This does not neccessarily limit to objects that can be displayed. Also
      abstract objects (as cameras) derive from this class.
      */
class LG_EXPORT LgObject3D : public QObject
{

    Q_OBJECT

    /**
          The x coodrinate of the object.
          */
    Q_PROPERTY(qreal x READ x WRITE setX)

    /**
          The y coodrinate of the object.
          */
    Q_PROPERTY(qreal y READ y WRITE setY)

    /**
          The z coodrinate of the object.
          */
    Q_PROPERTY(qreal z READ z WRITE setZ)




public:

    /*
              Constructor & Destructor
              */

    /**
              Constructor.
              Called to create a new object in 3D space. @p parent is the parent object to
              which the object will be added.
              */
    LgObject3D(QObject* parent = 0, qreal x = 0.0, qreal y = 0.0, qreal z = 0.0);

    /**
              Destructor.
              Called when the object is released.
              */
    virtual ~LgObject3D();



    /*
              Methods
              */

    /**
              Sets the object's position.
              Set the position of the object to the coordinates @p x, @p y and @p z.
              */
    void setPosition(qreal x, qreal y, qreal z);



    /*
              Getter and Setter
              */

    /**
              Getter for the x coordinate.
              */
    qreal x() { return m_X; }

    /**
              Setter for the x coordinate.
              Sets the x coordinate to @p x.
              */
    void setX(qreal x);

    /**
              Getter for the y coordinate.
              */
    qreal y() { return m_Y; }

    /**
              Setter for the y coordinate.
              Sets the y coordinate of the object to @p y;
              */
    void setY(qreal y);

    /**
              Getter for the z coordinate.
              */
    qreal z() { return m_Z; }

    /**
              Setter for the z coordinate.
              Sets the z coordinate of the object to @p z.
              */
    void setZ(qreal z);

    /**
       Is the object a visible one or a invisible entity.
       Subclasses should override this method depending on whether they
       need and/or can be drawn.
       @return True of the object is visible, or false if it is invisible.
       */
    virtual bool isVisible() { return false; }



private:

    qreal m_X;
    qreal m_Y;
    qreal m_Z;

    class Object3DPrivate;

    Object3DPrivate* d;

signals:

    /**
              Signal position changes of an object.
              This signal is emitted whenever the object moves.
              */
    void positionChanged();

};

#endif
