/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGANIMATIONFRAME_H
#define LGANIMATIONFRAME_H

/** @file
Provide the AnimationFrame class.
The AnimationFrame class represents a single frame in an animation. 
*/

#include <QObject>
#include "../lgmacros.h"
#include "lgtile.h"


/**
 The AnimationFrame class represents a single frame in an animation.
 This class provides the linkage between an Tile, the time it is displayed and
 a sub-tile (especially, if multiple frames are stored inside one big tile.
      */
class LG_EXPORT LgAnimationFrame : public QObject
{

    Q_OBJECT

    /**
   The tile. This will be displayed when the frame is shown in the animation.
   The tile can contain multiple subframes, which can be displayed using different rects.
 */
    Q_PROPERTY(LgTile* tile READ tile WRITE setTile)

    /**
   The amount of time, the frame is displayed. The value is in milliseconds.
 */
    Q_PROPERTY(int duration READ duration WRITE setDuration)

    /**
   The rectangle to display. This determines the width and height of the frame as well as the
   subtile inside tile.
 */
    Q_PROPERTY(QRect rect READ rect WRITE setRect)

public:

    /**
     The constructor.
     @p t is the tile this frame will display data from. @p d is the duration in milliseconds
     the frame will be displayed. @p r Specifies an rectangle, which can be used to specify an subframe
     from the given tile.
   */
    LgAnimationFrame(QObject* parent = NULL, LgTile* t = NULL, int d = 0, QRect r = QRect(0, 0, 0, 0));

    /**
     Destructor.
   */
    virtual ~LgAnimationFrame();

    /*
     Getter and Setter.
   */

    /**
     Getter for tile property. Returns the tile the frame is connected to.
   */
    LgTile* tile() { return m_Tile; }

    /**
     Setter for tile property. Sets the tile @p t the frame is connected to.
   */
    void setTile(LgTile* t);

    /**
     Getter for duration property. Returns the number of milliseconds, the frame will be displayed.
   */
    int duration() { return m_Duration; }

    /**
     Setter for duration property. Sets the time @p d in milliseconds the frame will be displayed.
   */
    void setDuration(int d);

    /**
     Getter for rect property. Returns the rectangle to display from tile.
   */
    QRect rect() { return m_Rect; }

    /**
     Setter for rect property. Sets the rectangle @p r to display from tile.
   */
    void setRect(QRect r);


private:

    class AnimationFramePrivate;
    AnimationFramePrivate* d;

    LgTile* m_Tile;
    int m_Duration;
    QRect m_Rect;
};


#endif
