/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgtexture.h"


class LgTexture::TexturePrivate
{
};




LgTexture::LgTexture(QObject* parent, GLuint textureId, bool delOnRelease) : QObject(parent)
{
  d = new TexturePrivate();
  m_Texture = 0;
  m_DeleteOnRelease = delOnRelease;
  setTexture(textureId);
}

LgTexture::~LgTexture()
{
  if (m_DeleteOnRelease)
  {
    releaseTexture();
  }
  delete d;
}

void LgTexture::bindTexture()
{
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, m_Texture);
}

void LgTexture::releaseTexture()
{
  glDeleteTextures(1, &m_Texture);
  m_Texture = 0;
}

void LgTexture::setTexture(GLuint texId)
{
  if (m_DeleteOnRelease && m_Texture != texId)
  {
    releaseTexture();
  }
  m_Texture = texId;
  if (m_Texture != 0)
  {
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &m_Width);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &m_Height);
  }
}

void LgTexture::setDeleteOnRelease(bool delOnRelease)
{
  m_DeleteOnRelease = delOnRelease;
}

