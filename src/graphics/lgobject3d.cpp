/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgobject3d.h"






class LgObject3D::Object3DPrivate {
};

LgObject3D::LgObject3D(QObject* parent, qreal x, qreal y, qreal z) : QObject(parent)
{
    d = new Object3DPrivate();
    m_X = x;
    m_Y = y;
    m_Z = z;
}

LgObject3D::~LgObject3D()
{
    delete d;
}

void LgObject3D::setPosition(qreal x, qreal y, qreal z)
{
    m_X = x;
    m_Y = y;
    m_Z = z;
    emit positionChanged();
}

void LgObject3D::setX(qreal x)
{
    m_X = x;
    emit positionChanged();
}

void LgObject3D::setY(qreal y)
{
    m_Y = y;
    emit positionChanged();
}

void LgObject3D::setZ(qreal z)
{
    m_Z = z;
    emit positionChanged();
}

