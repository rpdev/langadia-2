/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lganimationframe.h"

class LgAnimationFrame::AnimationFramePrivate
{
};





LgAnimationFrame::LgAnimationFrame (QObject* parent, LgTile* t, int d, QRect r ) : QObject (parent)
{
  m_Tile = t;
  m_Duration = d;
  m_Rect = r;

  this->d = new AnimationFramePrivate();
}

LgAnimationFrame::~LgAnimationFrame()
{
  delete d;
}

void LgAnimationFrame::setTile ( LgTile* t )
{
  m_Tile = t;
}

void LgAnimationFrame::setDuration ( int d )
{
 m_Duration = d;
}

void LgAnimationFrame::setRect ( QRect r )
{
  m_Rect = r;
}


