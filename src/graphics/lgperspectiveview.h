/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGPERSPECTIVEVIEW_H
#define LGPERSPECTIVEVIEW_H

/** @file
  Provides view class for perspective drawing.
*/

#include <QObject>
#include <QtOpenGL>
#include "../lgmacros.h"
#include "lgview.h"


/**
   View implementation providing perspective view.
   The PerspectiveView class subclasses the View class to
   provide perspective views, which usually is used for
   drawing in 3D space.
 */
class LG_EXPORT LgPerspectiveView : public LgView
{

    Q_OBJECT

    /**
       The view angle of the scene.
       The view angle used for drawing. Good values
       are between 45 and 60°.
     */
    Q_PROPERTY(qreal fovy READ fovy WRITE setFovy)

    /**
       The acpect ratio of the underlying widget.
       The acpect ratio of the widget onto which to draw. The acpect ratio
       can be calculated by dividing the widgets width by its height.
     */
    Q_PROPERTY(qreal aspectRatio READ aspectRatio WRITE setAspectRatio)

    /**
       The distance of the near clipping plane.
       Determines, how far the near clipping plane is away. Every fragment,
       whose z-value is below this value will be discarded.
     */
    Q_PROPERTY(qreal near READ near WRITE setNear)

    /**
       The distance of the far clipping plane.
       Determines, how far the far clipping plane is away. Every fragment,
       whose z-value is beyond this value will be discarded.
     */
    Q_PROPERTY(qreal far READ far WRITE setFar)

public:

    /*
                  Constructor & Destructor
                  */

    /**
    Constructor.
  */
    LgPerspectiveView(QObject* parent = 0, qreal fovy = 60.0, qreal aspratio = 1.0,
                    qreal near = 0.0, qreal far = 100.0);

    /**
    Destructor.
  */
    virtual ~LgPerspectiveView();

    /*
                  Methods
                  */

    void activate(QRect* rect = 0);

    /*
                  Getter & Setter
                  */

    /**
    Getter for the fovy property.
    Returns the view angle of the view.
  */
    qreal fovy() { return m_Fovy; }

    /**
    Setter for the fovy property.
    Sets the view angle for this view to @p fovy.
  */
    void setFovy(qreal fovy);

    /**
    Getter for the acpectRatio property.
    Returns the currently set aspect ratio value used for the
    view.
  */
    qreal aspectRatio() { return m_AspectRatio; }

    /**
    Setter for the aspectRatio property.
    Sets the aspect ratio to @p aspectRatio.
  */
    void setAspectRatio(qreal aspectRatio);

    /**
    Getter for the near property.
    Returns the current value of the near property.
  */
    qreal near() { return m_Near; }

    /**
    Setter for the near property.
    Sets the near clipping plane to @p near.
  */
    void setNear(qreal near);

    /**
    Getter for the far property.
    Returns the far clipping plane.
  */
    qreal far() { return m_Far; }

    /**
    Setter for the far property.
    Sets the far clipping plane to @p far.
  */
    void setFar(qreal far);

private:

    class PerspectiveViewPrivate;
    PerspectiveViewPrivate* d;

    qreal m_Fovy;
    qreal m_AspectRatio;
    qreal m_Near;
    qreal m_Far;

};

#endif // LGPERSPECTIVEVIEW_H
