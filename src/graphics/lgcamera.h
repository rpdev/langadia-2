/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef LGCAMERA_H
#define LGCAMERA_H

/** @file
  Provides base class for cameras.
  This file contains the base class for all cameras.
  */

#include <QObject>
#include <QtOpenGL>
#include "../lgmacros.h"
#include "lgobject3d.h"


/**
          The base class for all cameras.
          Camera is an abstract base for all camera objects. A camera itself is
          an object in 3D space, thus it extends the Object3D class. This makes it
          possible to treat cameras like ordinary entities.
          */
class LG_EXPORT LgCamera : public LgObject3D
{

    Q_OBJECT

public:

    /*
                 Constructor & Destructor
                 */

    /**
                 Constructor.
                 */
    LgCamera(QObject* parent = 0, qreal x = 0.0, qreal y = 0.0, qreal z = 0.0);

    /**
                 Destructor.
                 */
    virtual ~LgCamera();


    /*
                 Methods
                 */

    /**
                 Activate the camera.
                 Activates this camera. This basically sets the
                 projection matrix according to the cameras properties.
                 After a call to this method, the modelview matrix
                 will be activated.
                 */
    virtual void useCamera() = 0;

    /**
                 Save the currently set camera.
                 Pushes the currently set camera onto the matrix stack.
                 The matrix is reloaded using loadView().
                 */
    void saveView();

    /**
                 Loads a previously saved camera view.
                 Activates the matrix on the top of the camera stack
                 and removes it.
                 This method is used to restore a camera view saved with
                 saveView().
                 */
    void loadView();

private:

    class CameraPrivate;
    CameraPrivate* d;
};
#endif // LGCAMERA_H
