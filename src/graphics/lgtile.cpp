/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgtile.h"

class LgTile::TilePrivate
{
};





LgTile::LgTile(QObject* parent, GLuint textureId, bool delOnRelease):
  LgTexture(parent, textureId, delOnRelease)
{
  d = new TilePrivate();
}

LgTile::~LgTile()
{
  delete d;
}

/*
  Watch out!
  Windowing systems tend to place (0,0) in the top left corner of each component.
  OpenGL however places it in the bottom left corner of the screen!
  Thus, we have to flip top and bottom.
*/
void LgTile::draw(QRect rect, QColor color, QRect clip)
{
  glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF());
  bindTexture();
  int tW = width(), tH = height();
  if (tW == 0)
  {
    tW = 1;
  }
  if (tH == 0)
  {
    tH = 1;
  }
  
  GLfloat l = (float)clip.x()/(float)tW,
	  r = l + (float)clip.width()/(float)tW,
	  b = (float)clip.y()/(float)tH,
	  t = b + (float)clip.height()/(float)tH;
	  
  glBegin(GL_QUADS);
  glTexCoord2f(l, b);
  glVertex2i(rect.x(), rect.y());
  glTexCoord2f(r, b);
  glVertex2i(rect.x() + rect.width(), rect.y());
  glTexCoord2f(r, t);
  glVertex2i(rect.x() + rect.width(), rect.y() + rect.height());
  glTexCoord2f(l, t);
  glVertex2i(rect.x(), rect.y() + rect.height());
  glEnd();
}

void LgTile::draw ( QRect rect, QColor color )
{
  draw(rect, color, QRect(0, 0, width(), height()));
}

void LgTile::draw ( QRect rect )
{
  draw(rect, Qt::white);
}

void LgTile::draw ( int x, int y, QColor color, QRect clip )
{
  draw(QRect(x, y, width(), height()), color, clip);
}

void LgTile::draw ( int x, int y, QColor color )
{
  draw(x, y, color, QRect(0, 0, width(), height()));
}

void LgTile::draw ( int x, int y )
{
  draw(x, y, Qt::white);
}






