/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgperspectiveview.h"

#include "lgglwidget.h"

class LgPerspectiveView::PerspectiveViewPrivate
{
};




LgPerspectiveView::LgPerspectiveView(QObject* parent, qreal fovy, qreal aspratio,
                                          qreal near, qreal far) :
        LgView(parent)
{
    d = new PerspectiveViewPrivate();

    m_Fovy = fovy;
    m_AspectRatio = aspratio;
    m_Near = near;
    m_Far = far;
}

LgPerspectiveView::~LgPerspectiveView()
{
    delete d;
}

void LgPerspectiveView::activate(QRect* rect)
{
    qreal ar = m_AspectRatio;
    if (rect != NULL)
    {
        if (rect->height() != 0)
        {
            ar = (qreal)rect->width()/(qreal)rect->height();
            glViewport(rect->x(), rect->y(), rect->width(), rect->height());
        }

    }
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(m_Fovy, ar, m_Near, m_Far);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void LgPerspectiveView::setFovy(qreal fovy)
{
    m_Fovy = fovy;
}

void LgPerspectiveView::setAspectRatio(qreal aspectRatio)
{
    m_AspectRatio = aspectRatio;
}

void LgPerspectiveView::setNear(qreal near)
{
    m_Near = near;
}

void LgPerspectiveView::setFar(qreal far)
{
    m_Far = far;
}
