/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include "lgorthoview.h"

#include "lgglwidget.h"

class LgOrthoView::OrthoViewPrivate
{
};


LgOrthoView::LgOrthoView(QObject* parent, QRect screen, qreal near, qreal far): LgView(parent)
{
  d = new OrthoViewPrivate();
  m_Rect = screen;
  m_Near = near;
  m_Far = far;
}

LgOrthoView::~LgOrthoView()
{
  delete d;
}

void LgOrthoView::activate(QRect* rect)
{
  if (rect != NULL)
  {
    glViewport(rect->x(), rect->y(), rect->width(), rect->height());
  }
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if ((m_Rect.width() == 0 || m_Rect.height() == 0) && rect != NULL)
  {
    glOrtho(0.0, (GLdouble)rect->width(),
            0.0, (GLdouble)rect->height(),
	    m_Near, m_Far);
  }
  else
  {
    glOrtho((GLdouble)m_Rect.x(), (GLdouble)m_Rect.width(),
	    (GLdouble)m_Rect.y(), (GLdouble)m_Rect.height(),
	    m_Near, m_Far);
  }
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void LgOrthoView::setRect(QRect rect)
{
  m_Rect = rect;
}

void LgOrthoView::setNear(qreal near)
{
  m_Near = near;
}

void LgOrthoView::setFar(qreal far)
{
  m_Far = far;
}









