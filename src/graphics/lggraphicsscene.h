/*******************************************************************************
    Langadia++ 2 - A support framework for game development on top of Qt 4.x
    Copyright (C) 2009  RPdev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef LGGRAPHICSSCENE_H
#define LGGRAPHICSSCENE_H

/** @file
  Provides the GraphicsScene class for hardware accelerated
  drawing of scenes and Qt Widgets.
  */

#include <QObject>
#include <QGraphicsScene>
#include <QtOpenGL>
#include <QTime>
#include <QTimer>

#include "../lgmacros.h"
#include "lgview.h"

/**
      Provides hardware accelerated drawing.
      The GraphicsScene subclasses the QGraphicsScene class to provide
      hardware accelerated drawing of both 3D scenes and
      Qt widgets.This class can be used to display ordinary Qt widgets over a
      OpenGL drawn scene. The widgets are added using the methods derived
      from the QGraphicsScene class.
      OpenGL drawing can be archived by connecting to the signals provided
      by this class.
      */
class LG_EXPORT LgGraphicsScene : public QGraphicsScene
{

    Q_OBJECT

    /**
        Auto redrawing of the scene.
        This indicates, whether the scene updates periodically.
        Usually, this is needed when drawing OpenGL content (as e.g. animations
        would otherwise look bad).
        */
    Q_PROPERTY(bool autoRedraw READ autoRedraw WRITE setAutoRedraw)

    /**
        The minimal time between two updates.
        This is the time in milliseconds that pass, before the scene is updated using
        the autoRedraw mechanism. Note, that the real time can be longer.
        */
    Q_PROPERTY(int autoRedrawInterval READ autoRedrawInterval WRITE setAutoRedrawInterval)

    /**
        The view to use for 3D drawing.
        If this is set, the scene automatically activates the view before emitting the
        paint3D signal. This can be useful if only one view is used.
        */
    Q_PROPERTY(LgView* view READ view WRITE setView)

public:

    /*
          Constructor & Destructor
          */

    /**
          Constructor.
          */
    LgGraphicsScene(QObject* parent = 0);

    /**
          Destructor.
          */
    virtual ~LgGraphicsScene();

    /*
          Methods
          */

    /**
          Draw widget background.
          Draws the background of the scene. This is internally used
          to archive the OpenGL drawing. The @p painter can be used for
          plain 2D drawing. @p rect is the rectangular area to draw (usually,
          this should be the whole widget).
          */
    void drawBackground(QPainter *painter, const QRectF &rect);

    /*
          Getter & Setter
          */

    /**
          Automatic redrawing of widget.
          If autoRedraw is true, the widget uses an internal timer to periodically
          redraw its contents. This is usually required when drawing OpenGL contens.
          By default, auto redrawing is turned off.
          @sa autoRedrawInterval
        */
    bool autoRedraw() const { return m_Timer.isActive(); }

    /**
          Turn auto redrawing on or off.
          This method sets the auto redrawing mode of the widget to @p redraw. See also autoRedraw() for
          a more detailed description.
          @sa autoRedrawInterval
        */
    void setAutoRedraw(bool redraw);

    /**
          Interval between automatic redraws.
          This returns the time in milliseconds between two paint3D/paint2D signals.
          @sa autoRedraw
        */
    int autoRedrawInterval() const { return m_Timer.interval(); }

    /**
          Set the auto redraw interval.
          This sets the interval between to paint3D/paint2D signals to @p interval.
          @sa autoRedraw
        */
    void setAutoRedrawInterval(int interval);

    /**
          Getter for the view property. Returns the currently set view for the widget.
        */
    LgView* view() { return m_View; }

    /**
          Setter for the view property. Sets the view to use with this
          widget to @p view.
        */
    void setView(LgView* view);

private:

    class GraphicsScenePrivate;
    GraphicsScenePrivate* d;

    LgView* m_View;
    QTimer m_Timer;
    QTime m_Time;

    int m_LastDraw3D;
    int m_LastDraw2D;

signals:

    /**
          Initialize 3D drawing.
          This signal is emitted just before 3D drawing begins. It can be used to do one-time
          initialization (per render-loop).
        */
    void initialize3D();

    /**
          Resize view of the widget for 3D drawing.
          This is emitted just before 3D drawing. You can use it to set the viewport to the current
          widgets dimension, which is passed in through the parameters @p width and @p height.
        */
    void resize3D(int width, int height);

    /**
          Paint 3D scene.
          This is called to draw the 3D scene. Use it to paint the 3D stuff you want to display.
          Usually, the context should already been initialized, as initialize3D and resize3D have already been
          emitted. However, you can also perform initialization and resizing here.
          The @p advance parameter indicates the time since the last paint3D signal in milliseconds.
        */
    void paint3D(int advance);

    /**
          Paint 2D scene.
          Connect to this signal to draw 2D stuff onto the widget. Drawing should happen
          using the @p painter parameter. Note, that initialization and resizing has been done internally
          by the painter! If you want to draw using OpenGL directly, rather use the paint3D signal.
          There, you also can switch to 2D "view" (see glOrtho function) and perform drawing.
          The @p advance parameter indicates the time since the last paint2D signal.
        */
    void paint2D(QPainter* painter, int advance);

};

#endif
